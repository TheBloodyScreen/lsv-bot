import discord
from colorama import Fore
import wolframalpha
import wikipedia

token = "MzE2NTU5MTI2MzI0OTY5NDcy.DAXCLw.-d62bPGf55bBOgkpPM2hB5GvMco"

bot = discord.Client()


@bot.event
async def on_ready():
    print(Fore.GREEN + 'Logged in as')
    print(Fore.WHITE + "Name: " + Fore.GREEN + bot.user.name)
    print(Fore.WHITE + "ID: " + Fore.GREEN + bot.user.id)
    print(Fore.WHITE + '________________________')
    await bot.change_presence(game=discord.Game(name='LS-V | !info'))
    print(Fore.GREEN + "connected successfully")
    print(Fore.WHITE + '________________________')


@bot.event
async def on_message(message):
    author = message.author
    channel = message.channel
    test = channel.id == '158780065474150401'  # (TBS) this is just important for my testing env

    if message.content.startswith('!info'):
        print(Fore.CYAN + str(message.author) + " used !info in " + str(channel))
        await bot.delete_message(message)
        await bot.send_message(author, embed=discord.Embed(title='Los Santos V Info:', description='\nWebsite: <http://ls-v.com/>\n\nForum: \n<http://forum.ls-v.com/> \n\nBugtracker: \n<http://bug.ls-v.com/>\n\nServerlist: \nto be determined \n\nFAQ: \nto be determined', colour=0xF49700))
        await bot.send_message(author, embed=discord.Embed(title='Commandlist for this bot.', description="\n!info\nSends you these messages.\n\n!faq\nSends a link to the FAQ in the channel it has been used in.\n\n!status\nSends a development status message in the channel it has been used in.\n\n!bug\nSends a link to the Bugtracker in the channel it has been used in.", color=0x2AE92A))

    elif message.content.startswith('!faq'):
        print(Fore.CYAN + str(author) + " used !faq in " + str(channel))
        await bot.send_typing(channel)
        await bot.delete_message(message) 
        await bot.send_message(channel, embed=discord.Embed(title='You can find all the Information you need here:', description='indev please check back later', color=0xF49700))

    elif message.content.startswith('!status'):
        print(Fore.CYAN + str(author) + " used !beta in " + str(channel))
        await bot.send_typing(channel)
        await bot.delete_message(message)
        await bot.send_message(channel, embed=discord.Embed(title='Los Santos V Status', description='This project is currently in development.\nThere is no application form.\nFor more information please check the forums(!info).', color=0xF49700))

    elif message.content.startswith('!bug'):
        print(Fore.CYAN + str(author) + " used !bug in " + str(channel))
        await bot.send_typing(channel)
        await bot.delete_message(message)
        targetUser = message.content.replace('!bug', '')
        await bot.send_message(channel, targetUser + 'please use our Bug Tracker to report any issues you have found with the server or UCP')
        await bot.send_message(channel, embed=discord.Embed(title='You can find the bugtracker here:', description='http://bug.ls-v.com/', color=0xF49700))

    elif message.content.startswith('!?'):
        question = message.content.replace('!? ', '')
        question = str(question)
        print(question)
        await bot.send_typing(channel)
        try:
            appid = "HLA5QG-E9JTH4G73R"
            client = wolframalpha.Client(appid)
            res = client.query(question)
            answer = next(res.results).text
            await bot.send_message(channel, answer)
        except:
            try:
                answer = wikipedia.summary(question, sentences=3)
                await bot.send_message(channel, answer)
            except:
                await bot.send_message(channel, "Couldn't find what you are looking for. Sorry!")

    elif message.content.startswith('!test'):
        print(author)

    elif message.content.startswith('CODE'):
        await bot.delete_message(message)
        print(message.content)
        newMessage = str(message.content).replace("CODE", "```")
        sep = '#'
        author = str(author)
        authornick = author.split(sep, 1)[0]
        await bot.send_message(channel, str(authornick) + newMessage + "\n```")
        
    # (TBS) start of a function to delete links in any given channel
    # 
    #
    # elif message.content.find('http') >= 0 or message.content.find('.de') >= 0 or message.content.find('.com') >= 0 or message.content.find('.net') >= 0 or message.content.find('.org') >= 0 or message.content.find('.eu') >= 0:
    #     if guestroom:
    #         await bot.delete_message(message)
    #         print("Link by: " + str(author) + " deleted!")
    #         linkWarning = discord.Embed(title='Link Warnung.', description="Da der guestroom für alle user einzusehen ist, können wir es aus Sicherheitsgründen nicht erlauben dort links "
    #                                                                        "zu posten.", color=0x2AE92A)
    #         await bot.send_message(channel, embed=linkWarning)

bot.run(token)
